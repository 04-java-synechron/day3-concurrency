package countdownlatch;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public class CountdownLatchDemo {

    public static void main(String[] args) {
        CountDownLatch latch = new CountDownLatch(4);


        Runnable task = () -> {
            System.out.println("Started processing the job" + Thread.currentThread().getName());
            try {
                Thread.sleep(1000);
                latch.countDown();
            } catch (InterruptedException exception) {
                exception.printStackTrace();
            }

            System.out.println("Completed the job" + Thread.currentThread().getName());
        };

        Thread t1 = new Thread(task, "t1");
        Thread t2 = new Thread(task, "t2");
        Thread t3 = new Thread(task, "t3");
        Thread t4 = new Thread(task, "t4");

        t1.start();
        t2.start();
        t3.start();
        t4.start();

        try {
            latch.await(15, TimeUnit.SECONDS);
            System.out.println("Latch value " + latch.getCount());
            System.out.println(" Main thread wil move on ");
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        } finally {
            System.out.println(" Processed all the threads::");
        }
    }
}