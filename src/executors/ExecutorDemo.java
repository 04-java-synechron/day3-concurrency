package executors;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorDemo {

    public static void main(String[] args) {

        Runtime.getRuntime().availableProcessors();

        ExecutorService executorService = Executors.newSingleThreadExecutor();
       /* Thread t = new Thread();
        t.start();
*/
        Runnable task = () -> {
            try {
                Thread.sleep(2000);
                boolean flag = true;
               /* System.out.println("Processed the message :: "+ Thread.currentThread());
                if (flag)
                    throw new RuntimeException();*/
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        };

        executorService.submit(task);
        executorService.submit(task);

        executorService.shutdown();

        while ( !executorService.isTerminated()){
           // System.out.println("Waiting for the executor to shutdown");
        }
        System.out.println(" Now all the thread have completed the tasks ...");

    }
}