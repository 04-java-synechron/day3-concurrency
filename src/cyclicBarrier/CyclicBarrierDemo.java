package cyclicBarrier;

import java.util.concurrent.*;

public class CyclicBarrierDemo {

    public static void main(String[] args) throws InterruptedException, TimeoutException, BrokenBarrierException {

        CountDownLatch countDownForParty = new CountDownLatch(2);

        Runnable startParty = () -> {
            try {
                System.out.println(":::: Waiting for the party to start :: ");
                countDownForParty.await();
            } catch (InterruptedException exception) {
                exception.printStackTrace();
            }
            System.out.println("Start the party!!!!!");
        } ;

        Runnable orderPizza = () ->  {
            System.out.println("Pizza has ordered and will arrive soon :: ");
            countDownForParty.countDown();
        };
        Runnable orderCoke = () ->  {
            System.out.println("Coke has ordered and will arrive soon :: ");
            countDownForParty.countDown();
        };
        CyclicBarrier pizzaBarrier = new CyclicBarrier(4, orderPizza);
        CyclicBarrier cokeBarrier = new CyclicBarrier(2, orderCoke);

        Runnable inviteGuests = () -> {
            System.out.println("Guests have arrived :: "+ Thread.currentThread().getName());
            try {
                pizzaBarrier.await(4, TimeUnit.SECONDS);
                cokeBarrier.await(4, TimeUnit.SECONDS);
            } catch (InterruptedException | TimeoutException exception) {
                exception.printStackTrace();
            } catch (BrokenBarrierException e) {
                e.printStackTrace();
            }
        };

        Thread t1 = new Thread(inviteGuests, "Rakesh");
        Thread t2 = new Thread(inviteGuests, "Suresh");
        Thread t4 = new Thread(inviteGuests, "Mahesh");
        Thread t5 = new Thread(inviteGuests, "Vinay");
        Thread t6 = new Thread(inviteGuests, "Somesh");

        t1.start();
        t2.start();
        t4.start();
        t5.start();
        t6.start();

        new Thread(startParty, "Party Thread").start();

        System.out.println(" Main thread completed :: ");

    }
}