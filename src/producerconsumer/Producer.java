package producerconsumer;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.BlockingQueue;

public class Producer implements Runnable{

    private final BlockingQueue<Message> blockingQueue;


    public Producer(BlockingQueue<Message> blockingQueue) {
        this.blockingQueue = blockingQueue;
    }
    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            int delay  = 1000;
            Message message = new Message("Message "+ i, delay);

      //      synchronized (list){


            try {
                Thread.sleep(1000);
/*
                if (list.size()==4) {
                    list.wait();
                }
*/
                blockingQueue.put(message);

                System.out.println("Produced :: "+ message);
            } catch (InterruptedException exception){
                exception.printStackTrace();
            }
        }
    }
}