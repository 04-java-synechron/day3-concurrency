package producerconsumer;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

public class Message implements Delayed {

    private String message;
    private long startTime;

    public Message(String message, long delay){
        this.message = message;
        this.startTime = System.currentTimeMillis() + delay;
    }

    public String getMessage(){
        return this.message;
    }

    @Override
    public long getDelay(TimeUnit unit) {

        return unit.convert((startTime - System.currentTimeMillis()), TimeUnit.MILLISECONDS);

    }

    @Override
    public int compareTo(Delayed delayed) {
        if ( this.startTime < ((Message)delayed).startTime)
            return  - 1;
        else if ( this.startTime > ((Message)delayed).startTime)
            return 1;
        else
            return 0;
    }
}