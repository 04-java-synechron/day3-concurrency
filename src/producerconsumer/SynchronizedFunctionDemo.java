package producerconsumer;

import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Function;

public class SynchronizedFunctionDemo {
    private final ReentrantLock lock = new ReentrantLock();
    public int processDataSynchronously(Function<String, Integer> function, String input){
        try {
            lock.lockInterruptibly();
            //implement the function
            return function.apply(input);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }finally {
            lock.unlock();
        }
        return 0;
    }

    public static void main(String[] args) {
        SynchronizedFunctionDemo obj = new SynchronizedFunctionDemo();
        Function<String, Integer> lengthOfString = String::length;
        int result = obj.processDataSynchronously(lengthOfString, "Hello-world");
        System.out.println("Length of the input is "+ result);
    }
}