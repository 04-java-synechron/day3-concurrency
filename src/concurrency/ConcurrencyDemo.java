package concurrency;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import static java.lang.Thread.sleep;

public class ConcurrencyDemo {

    public static void main(String[] args) throws InterruptedException {
        Date todayDate = new Date();


       /* LocalDate localDate = todayDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        Calendar calendar = Calendar.getInstance();

        LocalDate currentDate = LocalDate.now();

        LocalDate newDate = currentDate.minus(2, ChronoUnit.DAYS);

        LocalDateTime tim = LocalDateTime.now();

        System.out.println(Objects.equals(null, null));

        int[] ages = {11,22,33,42,434};
        Arrays.stream(ages);
*/
        Printer printer = new Printer(5);

        Thread t1 = new Thread(new Task(printer), "t1");
        Thread t2 = new Thread(new Task(printer), "t2");
        Thread t3 = new Thread(new Task(printer), "t3");

        t1.start();
        t2.start();
        t3.start();

        t1.join();
        t2.join();
        t3.join();

        System.out.println(" The main thread will wait for all the threads to complete the job....");
    }

}

class Printer {

    private final int count;
 /*   private final ReentrantLock lock1 = new ReentrantLock();
    private final ReentrantLock lock2 = new ReentrantLock();
    private final ReentrantLock lock3 = new ReentrantLock();*/

    public Printer(int number){
        this.count = number;
    }
    public synchronized void print(){
        try {
            for(int i = count; i > 0; i-- ){
                Thread.sleep(1000);
                boolean flag = false;
               /* if (!flag){
                    throw new IllegalArgumentException("Exception thrown inside the run method");
                }*/
                System.out.println("Printing :: "+ i + " Thread :: "+ Thread.currentThread().getName());
            }
            System.out.println("printing completed");
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
    }
}

class Task implements Runnable {

    private Printer printer;

    public Task(Printer printer){
        this.printer = printer;
    }
    @Override
    public void run() {
            printer.print();
    }
}