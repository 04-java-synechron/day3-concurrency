package completablefuture;


/*
    Resolution rules:
    1. Classes always win
    2. Closest sub-interface win
 */
public class DefaultMethodResolution extends X implements A, B, C {

/*
    public void hello() {
        System.out.println(" Inside the DefaultMethodResolution");
    }
*/

    public static void main(String[] args) {
        new DefaultMethodResolution().hello();
    }
}

interface A extends C {
    default void hello() {
        System.out.println(" Hello from A");
    }
}

interface B extends A {
    default void hello() {
        System.out.println(" Hello from B");
    }
}

interface C  {
    default void hello() {
        System.out.println(" Hello from C");
    }
}

class X  {
    public void hello() {
        System.out.println(" Inside X");
    }
}