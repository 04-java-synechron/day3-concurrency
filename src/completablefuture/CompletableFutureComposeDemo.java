package completablefuture;

import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CompletableFutureComposeDemo {

    public static List<String> getProducts(){
        delay(12);
        return List.of("IPhone", "Macbook-Pro", "Thinkpad", "Thinkbook", "mouse");
    }

    public static Double getPriceAsAyncReturningPrice(String product){
        delay(2);
        System.out.println(" Inside getPriceAsyc :: " + product);
        return 25_000D;
    }

    public static Double currencyExchange(String country, double price ){
        delay(4);
        System.out.println(" Inside currencyExchange :: " + price);
        return  4 * price;
    }
    public static String writeToOutStream(double outputValue ){
        delay(5);
        System.out.println(" Writing the data to disk :: "+ outputValue);
        return  "Completed the execution";
    }

    private static final void delay(int timeout) {
        try {
            System.out.println("Delay ::");
            Thread.sleep(timeout * 1000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
    }


    public static void main(String[] args) {

        System.out.println("Main thread started");

/*
        CompletableFuture<Double> productPrice = CompletableFuture.supplyAsync(() -> getPriceAsAyncReturningPrice("IPad"));
        CompletableFuture<Double> productPriceINUSD = productPrice.thenApply(output -> currencyExchange("IN", output));
        CompletableFuture<String> completableFutureDiskIO = productPriceINUSD.thenApply(output -> writeToOutStream(output));
        CompletableFuture<Void> competedFuture = completableFutureDiskIO.thenRun(() -> System.out.println(" Stream processing is complete"));

*/

/*

        CompletableFuture<Void> completableFuture = CompletableFuture.supplyAsync(() -> getPriceAsAyncReturningPrice("IPad"))
                .thenApply(output -> currencyExchange("IN", output))
                .thenApply(output -> writeToOutStream(output))
                .thenRun(() -> System.out.println(" Stream processing is complete"));

        System.out.println("Main thread is free to have a cup of coffee !! ");
        completableFuture.join();
*/

     /*   ExecutorService executorService = Executors.newFixedThreadPool(10);
        try{
            Future<?> task1 = executorService.submit(() -> {
                try {
                    Thread.sleep(1000);
                    System.out.println(" Completed task-1");
                } catch (InterruptedException exception) {
                    exception.printStackTrace();
                }
            });

            Object output = task1.get(20, TimeUnit.SECONDS); //blocking opeartion
            Future<?> task2 = executorService.submit(() -> {
                try {
                    Thread.sleep(1000);
                    //output.
                    System.out.println(" Completed task-1");
                } catch (InterruptedException exception) {
                    exception.printStackTrace();
                }
            });

            task2.get(); //blocking opeartion

        } catch (Exception exception){
            System.out.println(exception.getMessage());
        }
    }*/

        ExecutorService cpuIntensive = Executors.newFixedThreadPool(12);
        ExecutorService ioIntensive = Executors.newFixedThreadPool(100);
        for(String product: getProducts()){
            CompletableFuture.supplyAsync(() -> getPriceAsAyncReturningPrice(product), ioIntensive)
                    .thenApplyAsync(output -> currencyExchange("IN", output), cpuIntensive)
                    .thenApply(output -> writeToOutStream(output))
                    .thenRun(() -> System.out.println(" Stream processing is complete"));
        }

        CompletableFuture<Double> fetchPriceFromAmazon = null;
        CompletableFuture<Double> fetchPriceFromFlipkart = null;
        CompletableFuture<Double> fetchPriceFromMyntra = null;

        CompletableFuture.allOf(fetchPriceFromAmazon, fetchPriceFromFlipkart, fetchPriceFromMyntra);
        CompletableFuture.anyOf(fetchPriceFromAmazon, fetchPriceFromFlipkart, fetchPriceFromMyntra);
/*        //reduce
        fetchPriceFromAmazon.thenCombine(data -> );
        //flatmap
        fetchPriceFromAmazon.thenCompose()*/
    }
}