package completablefuture;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class CompletableFutureDemo {

    public CompletableFuture<Double> getPriceAsAync(String product){
        CompletableFuture<Double> priceOfProduct = new CompletableFuture<>();
        new Thread(() -> {
                delay();
                priceOfProduct.complete(Math.ceil(product.length() * 10000 * Math.random()));
        }).start();
        return priceOfProduct;
    }

    public CompletableFuture<Double> getPriceAsAyncWithException(String product){
        CompletableFuture<Double> priceOfProduct = new CompletableFuture<>();
        new Thread(() -> {
                boolean flag = true;
                try {
                    if (flag) {
                        throw new IllegalArgumentException("invalid data");
                    }
                    delay();
                    priceOfProduct.complete(Math.ceil(product.length() * 10000 * Math.random()));
                }catch (Exception e) {
                    priceOfProduct.completeExceptionally(e);
                    e.printStackTrace();
                }
        }).start();
        return priceOfProduct;
    }

    public Double getPriceAsAyncReturningPrice(String product){
        delay();
        return product.length() * 45D;
    }

    public static void delay() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
    }

    public CompletableFuture<List<String>> fetchAllUsers() {
        delay();
        CompletableFuture<List<String>> users = new CompletableFuture<>();
        new Thread(() -> {

            users.complete(List.of("Harish", "Suresh", "Vinay"));
        }).start();
        return  users;
    }
    public CompletableFuture<List<String>> fetchAllAcccounts() {
        delay();
        CompletableFuture<List<String>> accounts = new CompletableFuture<>();
        new Thread(() -> {
            try {
                System.out.println(" This is the child thread :: " + Thread.currentThread().getName());
                accounts.complete(List.of("1111- Harish", "2222-Vinay", "33333-Suresh"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
        return  accounts;
    }


    public static void main(String[] args) throws ExecutionException, InterruptedException {
        CompletableFutureDemo obj = new CompletableFutureDemo();
        //        CompletableFuture<Double> macBookProPrice = obj.getPriceAsAync("MacBookPro");
        //
        /*obj.getPriceAsAync("thinkpad")
                .completeAsync(() ->{
                    //making the rest api call for fetching the accounts
                    return 34;
        });*/
        System.out.println(" Main thread is printing");
        /*
          imperative style
        CompletableFuture<Double> productPrice = obj.getPriceAsAync("IPad");
        //Double blockingResult = productPrice.get();//blocking operation
        //System.out.println("Result :: "+ blockingResult);
        productPrice.thenAccept(result -> System.out.println( " result is "+ result));
        */

        // declarative style
       /* obj.getPriceAsAync("IPad").thenAccept(result -> System.out.println(result));
        System.out.println(" Main thread has terminated");*/
        //exception handling

      /* 
       CompletableFuture<Double> priceAsAyncWithException = obj.getPriceAsAyncWithException("Mac-book pro");
        priceAsAyncWithException
                .exceptionally(exception -> {
                    System.out.println(exception.getMessage());
                    return 45D;
                })
                .thenAccept(result -> System.out.println(" Reusult of the operation :: "+ result));*/

        CompletableFuture<Void> cf = CompletableFuture.supplyAsync(() -> obj.getPriceAsAyncReturningPrice("Iphone"))
                .exceptionally(e -> 56D)
                .thenAccept(result -> System.out.println(result));

        System.out.println("Main method is terminated");
        cf.join(); //blocking
    }
}