package streams;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.*;

public class GroupingDemo {

    public static void main(String[] args) {
        Stream<Dish> dishes = Stream.of(
                new Dish("upma", 200, true),
                new Dish("dosa", 300, true),
                new Dish("idli", 40, true),
                new Dish("egg-rice", 400, false),
                new Dish("chicken-rice", 500, false)
        );
         Stream<Dish> dishesWithTypes = Stream.of(
                        new Dish("upma", 200, DishType.KARNATAKA),
                        new Dish("special-dosa", 400, DishType.KARNATAKA),
                        new Dish("masala-dosa", 400, DishType.KARNATAKA),
                        new Dish("idli", 40, DishType.KERALA),
                        new Dish("egg-rice", 400, DishType.MAHARASHTRA),
                        new Dish("chicken-rice", 500, DishType.MAHARASHTRA)
                );



         //default output
         Map<DishType, List<Dish>> defaultGroupingOperation;
         Map<DishType, String> customSorting;




       // Map<Boolean, Long> totalDishCountByCalories = dishes.collect(Collectors.groupingBy(dish -> dish.getCalories() < 100, Collectors.counting()));
        //Map<DishType, Long> mapByDishType = dishesWithTypes.collect(Collectors.groupingBy(dish -> dish.getDishType(), Collectors.counting()));
        //Map<DishType, List<Dish>> collect = dishesWithTypes.collect(Collectors.groupingBy(dish -> dish.getDishType()));

        //dishes.collect(Collectors.partitioningBy(Dish::isVegetarion),Collectors.collectingAndThen(maxBy(comparingInt(Dish::getCalories)), Optional::get)) );

        // Parition the Dish based on the veg/non-veg and group them with highest calories.
       /* dishes.collect(Collectors.partitioningBy(Dish::isVegetarion),
                Collectors.collectingAndThen(maxBy(comparingInt(Dish::getCalories)), Optional::get)));*/

/*
        Optional<Dish> optionalDish = Optional.empty();
        optionalDish.get();
*/
        //dishes.collect(Collectors.partitioningBy(Dish::isVegetarion, Collectors.collectingAndThen(maxBy(comparingInt(Dish::getCalories)), Optional::get)));

        Stream<Stock> stocks = Stream.of(new Stock("TCS", 200, StockType.IT), new Stock("INF", 400, StockType.IT), new Stock("TCS", 600, StockType.IT), new Stock("INF", 100, StockType.IT));


        Map<String, Double> updatedStockPriceMap;

        // Get the number of thread
        Runtime.getRuntime().availableProcessors();
        Predicate<Stock> predicateBYPrice = stock -> stock.getPrice() < 400;
        Predicate<Stock> predicateBYName = stock -> stock.getSymbol().equals(StockType.IT);
        stocks
                .parallel()
                .filter(predicateBYPrice.and(predicateBYName))
                .map(Stock::getPrice)
                .sequential()
                .distinct()
                .findFirst();
    }
}

class Stock {
    private String symbol;
    private double price;
    private StockType stockType;

    public Stock(String symbol, double price, StockType stockType) {
        this.symbol = symbol;
        this.price = price;
        this.stockType = stockType;
    }

    public String getSymbol() {
        return symbol;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Stock{" +
                "symbol='" + symbol + '\'' +
                ", price=" + price +
                '}';
    }
}

enum StockType {
    IT,
    PHARMA,
    FMCG
}