package streams;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class DefaultAndStaticMethods {

    public static void main(String[] args) {
        Arrays.asList("a", "b","c");
       List.of(2,3,4,5);
    }
}

class CustomIterator implements Iterator<String> {

    @Override
    public boolean hasNext() {
        return false;
    }

    @Override
    public String next() {
        return null;
    }

}