package streams;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.function.BinaryOperator;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

public class StreamDemo {

    public static void main(String[] args) {
        Stream<Dish> dishes = Stream.of(new Dish("upma", 200, true), new Dish("dosa", 300, true), new Dish("idli", 40, true));
/*
        dishes.filter(dish -> dish.getCalories() > 100).forEach(System.out::println);
        boolean isThereAnyVeg = dishes.anyMatch(Dish::isVegetarion);
        boolean areAllVeg = dishes.allMatch(Dish::isVegetarion);
        boolean noneOfVeg = dishes.noneMatch(Dish::isVegetarion);
        System.out.println(isThereAnyVeg);

        Stream<Integer> numbers = Stream.of();

        Optional<Integer> maxOptional = numbers.reduce(Integer::max);
        maxOptional.ifPresent(integer -> System.out.println("Maximum  value" + integer));

        Optional<Integer> minOptional = numbers.reduce(Integer::min);

        Optional<Dish> optionalDish = dishes.findAny();*/

        Optional<Integer> optionalDishCalories = dishes
                                                   .filter(dish -> !dish.isVegetarion())
                                                   .findAny()
                                                   .map(Dish::getCalories);

        OptionalDouble maxCalories = dishes
                                        .mapToDouble(Dish::getCalories)
                                        .max();
        List<Integer> numbers = getNumbers(false);

        numbers.size();

        for(int v: getNumbers(true)){

        }
    }

    public static List<Integer> getNumbers(boolean flag){

        if(flag) {
            return List.of(1,2,3,4);
        }
        return new ArrayList<>();
    }
}

