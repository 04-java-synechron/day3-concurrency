package streams;

public class Stock1 {

    String name;
    int price;
    public Stock1(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Stock1{" +
                "name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}

