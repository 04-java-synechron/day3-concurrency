package streams;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StockStreamsDemo {
    public static void main(String[] args) {

/*
        Map<String, Optional<Stock1>> collect = Stream.of(new Stock1("TCS", 100),
                new Stock1("TCS", 200), new Stock1("INFY", 150),
                new Stock1("TCS", 600), new Stock1("INFY", 950),
                new Stock1("INFY", 800), new Stock1("NOKIA", 100)).

                collect(Collectors.groupingBy(x -> ((Stock1) x).getName(), Collectors.reducing(StockStreamsDemo::maxOperator)));
*/

        Map<String, Optional<Stock1>> result = Stream.of(new Stock1("TCS", 100),
                new Stock1("TCS", 200), new Stock1("INFY", 150),
                new Stock1("TCS", 600), new Stock1("INFY", 950),
                new Stock1("INFY", 800), new Stock1("NOKIA", 100)).

                collect(Collectors.groupingBy(x -> ((Stock1) x).getName(), Collectors.reducing(StockStreamsDemo::maxOperator)));

        System.out.println(result);

    }

    private static Stock1 maxOperator(Stock1 x, Stock1 y) {
        return x.getPrice() < y.getPrice() ? x : y;
    }
}