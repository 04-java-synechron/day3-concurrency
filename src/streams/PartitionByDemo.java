package streams;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PartitionByDemo {

    public static void main(String[] args) {
        Stream<Dish> dishesWithTypes = Stream.of(
                new Dish("upma", 200, DishType.KARNATAKA),
                new Dish("dosa", 300, DishType.KARNATAKA),
                new Dish("idli", 40, DishType.KERALA),
                new Dish("egg-rice", 400, DishType.MAHARASHTRA),
                new Dish("chicken-rice", 500, DishType.MAHARASHTRA)
        );

        Map<Boolean, List<Dish>> dishesParitionedByCaloriesLessThan100 = dishesWithTypes.collect(Collectors.partitioningBy(dish -> dish.getCalories() < 100));
        System.out.println(dishesParitionedByCaloriesLessThan100);



    }
}