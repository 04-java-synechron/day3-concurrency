package streams;

class Dish {
    private String name;
    private int calories;
    private boolean vegetarion;
    private DishType dishType;

    public Dish(String name, int calories, boolean vegetarion){
        this.name = name;
        this.calories = calories;
        this.vegetarion = vegetarion;
    }
    public Dish(String name, int calories, DishType dishType){
        this.name = name;
        this.calories = calories;
        this.dishType = dishType;
    }

    public String getName() {
        return name;
    }

    public int getCalories() {
        return calories;
    }

    public boolean isVegetarion(){
        return this.vegetarion;
    }
    public DishType getDishType(){
        return this.dishType;
    }

    @Override
    public String toString() {
        return "Dish{" +
                "name='" + name + '\'' +
                ", calories=" + calories +
                ", vegetarion=" + vegetarion +
                '}';
    }
}