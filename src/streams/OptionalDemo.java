package streams;

import java.util.Optional;
import java.util.stream.Stream;

public class OptionalDemo {

    public static void main(String[] args) {
        UserRepository repository = new UserRepository();
        Optional<String> optionalUser = repository.fetchUserByUserId(23);
        optionalUser.ifPresent(user -> System.out.println());
        OptionalDemo demo = new OptionalDemo();
        optionalUser.ifPresentOrElse(demo::processData, () -> System.out.println(" Logging that the user is not present"));
        boolean isUserPresent = optionalUser.isPresent();

        optionalUser.ifPresent(System.out::println);

        if (isUserPresent){
            User user = new User();
            Stream.of(user)
                  .map(User::getName)
                  .forEach(u -> System.out.println(u));
        }

        Stream<String> fruits = Stream.of("apple", "oranges", "banana", "papaya");

        fruits.map(String::toUpperCase)
                .forEach(System.out::println);

    }

    private void processData(String user) {
        System.out.println(user);
    }
}

class User {
    private String name;

    public String getName(){
        return  this.name;
    }
}


class UserRepository {

    public Optional<String> fetchUserByUserId(long userId){
        //talk to the db
        //1. the user is not present
        //2. The user is present
        return Optional.of("vinay");
    }
}