package generics;

import java.util.ArrayList;
import java.util.List;

public class GenericsDemo<E> {

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        GenericsDemo obj = new GenericsDemo();
        obj.print("Hello-world");
    }

    public static <T extends Number> T binaryOperator(T operand1, T operand2) {

        return (T) Integer.valueOf(operand1.intValue() + operand2.intValue());
    }

    public static <T extends Number> T unaryOperator(T operand1) {
        return (T) Integer.valueOf(operand1.intValue() * 4);
    }


    public void  print(E str){
       // System.out.println(str.toUpperCase());

    }
}

class IntegerPrinter implements PrintInterface {

    @Override
    public void print(Object obj) {
         //   System.out.println(((Integer)obj).intValue());
    }
}

interface PrintInterface {
    void print(Object t);
}