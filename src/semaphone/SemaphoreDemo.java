package semaphone;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class SemaphoreDemo {

    public static void main(String[] args) throws InterruptedException {

        Semaphore semaphore = new Semaphore(3);
        CountDownLatch countDownLatch = new CountDownLatch(4);

        Runnable task = () -> {
            try {
                semaphore.tryAcquire(10, TimeUnit.SECONDS);
                System.out.println(" Acquiring the lock: " + Thread.currentThread().getName());
                System.out.println("Total number of permits "+ semaphore.availablePermits());
                Thread.sleep(2000);
                System.out.println("Completing the job:: " + Thread.currentThread().getName());
            } catch (InterruptedException exception) {
                exception.printStackTrace();
            } finally {
                semaphore.release();
                countDownLatch.countDown();
                System.out.println("Total number of permits after releasing "+ semaphore.availablePermits());
            }
        };



        Thread t1 = new Thread(task, "Praveen");
        Thread t2 = new Thread(task, "Ramesh");
        Thread t3 = new Thread(task, "Suresh");
        Thread t4 = new Thread(task, "HariPrasad");

        t1.start();
        t2.start();
        t3.start();
        t4.start();

      /*  t1.join();
        t2.join();
        t3.join();
        t4.join();

*/
        countDownLatch.await(10, TimeUnit.SECONDS);

        System.out.println("Completed the main job");
    }
}