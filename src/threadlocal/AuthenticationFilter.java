package threadlocal;

import java.util.UUID;

public class AuthenticationFilter {

    public void intercepts (String username, String password) throws InterruptedException {
        Thread.sleep(2000);
        String uuid = UUID.randomUUID().toString() + "-"+ Thread.currentThread().getName();
        System.out.println(" UUID generated for Thread : "+ Thread.currentThread().getName() + " username :: "+ username + " UUID :: "+ uuid);
        UserDetails userDetails = new UserDetails(username, uuid , true);
        SecurityContextHolder.setUserContext(userDetails);
    }

    private String fetchUserPassword() {
        return "welcome";
    }

    private String fetchUserName() {
        return "ramesh";
    }
}