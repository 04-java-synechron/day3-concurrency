package threadlocal;

import javax.swing.undo.AbstractUndoableEdit;

public class TheadLocalClient {

    public static void main(String[] args) throws InterruptedException {
        //main thread
        Runnable task = () -> {
            AuthenticationFilter authenticationFilter = new AuthenticationFilter();
            try {
                authenticationFilter.intercepts("ramesh "+Thread.currentThread().getName(), "kumar");
            } catch (InterruptedException exception) {
                exception.printStackTrace();
            }
            System.out.println("ThreadSafeToken :: "+ Thread.currentThread().getName()+ " ::" +SecurityContextHolder.getUserContext().get("ThreadLocal"+Thread.currentThread().getName()).getAuthToken());

            System.out.println("NonThreadSafeToken :: "+ Thread.currentThread().getName()+ " ::" +SecurityContextHolder.getUserContext().get("NonThreadLocal"+Thread.currentThread().getName()).getAuthToken());
        };


            new Thread(task, "thread-1").start();
            new Thread(task, "thread-2").start();


        Thread.sleep(10_000);
    }
}