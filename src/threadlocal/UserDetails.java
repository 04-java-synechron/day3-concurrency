package threadlocal;

public final class UserDetails {

    private String username;
    private String authToken;
    private boolean authenticated;

    public UserDetails() {

    }

    public UserDetails(String username, String authToken, boolean authenticated) {
        this.username = username;
        this.authToken = authToken;
        this.authenticated = authenticated;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }

    @Override
    public String toString() {
        return "UserDetails{" +
                "username='" + username + '\'' +
                ", authToken='" + authToken + '\'' +
                ", authenticated=" + authenticated +
                '}';
    }
}