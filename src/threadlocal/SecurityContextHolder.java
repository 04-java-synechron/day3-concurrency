package threadlocal;

import java.util.HashMap;
import java.util.Map;

public class SecurityContextHolder {

    private static final ThreadLocal<UserDetails> userDetailsHolder = new ThreadLocal<>();
    private static UserDetails nonThreadLocalUserDetails = null;

    public static final Map<String, UserDetails> getUserContext(){
        UserDetails userDetails = userDetailsHolder.get();
        Map<String, UserDetails> resultMap = new HashMap<>();
        if (userDetails == null){
            userDetails = new UserDetails();
            userDetailsHolder.set(userDetails);
        }
        resultMap.put("ThreadLocal"+Thread.currentThread().getName(), userDetailsHolder.get());
        resultMap.put("NonThreadLocal"+Thread.currentThread().getName(), nonThreadLocalUserDetails);
        System.out.println("========================================");
        System.out.println(resultMap);
        System.out.println("========================================");
        return resultMap;
    }
    public static final void setUserContext(UserDetails userDetails){
        userDetailsHolder.set(userDetails);
        nonThreadLocalUserDetails = userDetails;
    }
}